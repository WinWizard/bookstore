/*********************************************************
 * File Name: BookStore.java 
 * Author: Dylan Kozicki, Zachary McMinn, Ryan Ousey 
 * Date of last revision: 10/7/2014
 * 
 * Short Description: A console-driven menu-based program that allow the 
 *                    user to build a cart of items and leave with a receipt
 **********************************************************/
package bookstore;

import java.util.ArrayList;
import java.util.Scanner;

public class BookStore {

    static ArrayList<Item> cart = new ArrayList<>();
    static ArrayList<Item> inv = new ArrayList<>();

    public static void main(String[] args) {
        initializeItems();
        Scanner scn = new Scanner(System.in);
        boolean run = true;
        int menuInt, index = -1;
        String menuChoice = "-1"; //initiialize menu choice
        while (run) {//loop to run the program stops when the user decides to exit
            System.out.print("     MAIN MENU\n--------------------\n"
                    + "1) Add Item to Cart\n2) Manage Cart\n3) Print shop items\n4) Print cart\n5) Print recipt\n6) Exit\n> ");
            boolean isValid; //boolean to check valid homework
            do {
                isValid = true;
                switch (scn.nextLine()) { //get the input from user and determine the choice
                    case "1": //adds item to the cart
                        addItem();
                        break;
                    case "2": //enables you to edit the cart
                        manageCart();
                        break;
                    case "3": //prints the inventory
                        printInv();
                        break;
                    case "4": //prints the cart
                        printCart();
                        break;
                    case "5": //prints the recipt
                        printRecipt();
                        break;
                    case "6": //exits and prints the recipt
                        printRecipt();
                        System.out.println("Thank you for shopping with us, have a nice day!");
                        run = false;
                        break;
                    default: //invalid input case
                        isValid = false;
                        System.out.print("> ");
                }
            } while (!isValid);
        }
    }

    static void initializeItems() { //initializes the items the shop has
        inv.add(new Book("A Game of Thrones", 4.99, false, true, true, "George RR Martin"));
        inv.add(new Book("The Name of the Wind", 10.99, true, false, true, "Patrick Rothfuss"));
        inv.add(new Book("A Discovery of Witches", 19.99, true, true, true, "Deborah Harkness"));
        inv.add(new Book("The Blade Itself", 9.60, false, false, true, "Joe Abercrombie"));
        inv.add(new Book("Magnets...How Do They Work?", 14.95, true, true, false, "Elvis Stevens"));
        inv.add(new Book("Thaumonomicon", 100.00, true, false, false, "Azanor the Void Walker"));
        inv.add(new Book("Thinking With PoRTaLS", 4.50, false, false, false, "Aperture Science"));
        inv.add(new Book("The Legend of Kara Tul", .99, true, false, true, "Kara Tul"));
        inv.add(new Book("Visions of Bravery", 8.49, false, true, true, "Elanor the Brave"));
        inv.add(new Book("Childen of Morta", 6.66, true, true, false, "Frankling Zappa"));
        inv.add(new BoosterPack("Magic The Gathering", 6.99, "Dark Ascension"));
        inv.add(new BoosterPack("Yu Ghi Oh!", 12.99, "Secrets of Eternity"));
        inv.add(new BoosterPack("Cardfight!! Vanguard", 4.95, "A Banquet of Divas"));
        inv.add(new BoosterPack("Pokemon", 10.98, "Johto League"));
        inv.add(new BoosterPack("Weiss Schwarz", 19.99, "Sword Art Online"));
    }

    static void printInv() { //prints the inventory list
        for (int i = 0; i < inv.size(); i++) {

            System.out.println(i + ") " + inv.get(i).toString());
        }
    }

    static void printBooks() { //prints the books
        for (int i = 0; i < inv.size(); i++) {
            if (inv.get(i) instanceof Book) {
                System.out.println(i + ") " + inv.get(i).toString());
            }
        }
    }

    static void printBoosterPacks() { //prints the booster packs
        for (int i = 0; i < inv.size(); i++) {
            if (inv.get(i) instanceof BoosterPack) {
                System.out.println(i + ") " + inv.get(i).toString());
            }
        }
    }

    static void printCart() { //prints the cart
        for (int i = 0; i < cart.size(); i++) {
            System.out.println(i + ") " + cart.get(i).toString() + "QTY: " + cart.get(i).getQuantity() + "\n");
        }
        if (cart.isEmpty()) { //check if the cart is empty
            System.out.println("Cart is Empty\n");
        }
    }

    static void addItem() { //adds item to cart
        Scanner scn = new Scanner(System.in);
        int choice;
        String input;

        do {
            choice = -1;
            printInv();
            System.out.print((inv.size()) + ") Return to Main...\n\nWhich item would you like to add: ");

            input = scn.nextLine();

            if ((int) (input.charAt(0)) >= 48 && (int) (input.charAt(0)) <= 57) { //determine if the first character is a numerical value so that the program doesnt break
                choice = Integer.parseInt(input);

            }
            if (choice < 0 || choice > (inv.size())) { //determine invalid input
                System.out.print("\nInvalid Input...\n\n");
            }
        } while (choice < 0 || choice > (inv.size()));

        if (choice == (inv.size())) {
            return;
        }

        Item i = inv.get(choice); //creates a temporary item for the choice

        i.addQuantity(1); //add 1 to the item
        if (cart.indexOf(i) == -1) { //add to the cart if it is not already there
            cart.add(i);
        }

        System.out.print("\nItem Added...\n");
    }

    static void manageCart() { //manages cart
        Scanner scn = new Scanner(System.in); 
        int choice1, choice2;
        String input;

        do { //loop while input is not valid to determine which item that the user wants to edit
            choice1 = -1;
            printCart();
            System.out.print((cart.size()) + ") Return to Main...\n Pick the Item you want to edit:");

            input = scn.nextLine();

            if ((int) (input.charAt(0)) >= 48 && (int) (input.charAt(0)) <= 57) { //determine if the first character is a numerical value so that the program doesnt break
                choice1 = Integer.parseInt(input);

            }
            if (choice1 < 0 || choice1 > (cart.size())) { //check for invalid input
                System.out.print("\nInvalid Input...\n\n");
            }
        } while (choice1 < 0 || choice1 > cart.size());

        if (choice1 == cart.size()) {
            return;
        }

        do {//loop for input to check whether it is an integer
            choice2 = -1;

            System.out.print("\n\nWhat would you like to do with this item?\n"
                    + "0) Change Quantity\n1) Remove Item\n2) Return to Main\n> ");

            input = scn.nextLine();

            if ((int) (input.charAt(0)) >= 48 && (int) (input.charAt(0)) <= 50) { //determine if the first character is a numerical value so that the program doesnt break
                choice2 = Integer.parseInt(input);

            } else {
                System.out.print("\nInvalid Input...\n\n");
            }
        } while ((int) (input.charAt(0)) < 48 || (int) (input.charAt(0)) > 50);

        switch (choice2) { //determines the choice
            case 0: //change quantity
                do {
                    System.out.print("\n\nEnter the new quantity: ");

                    input = scn.nextLine();

                    if ((int) (input.charAt(0)) >= 48 && (int) (input.charAt(0)) <= 57) {//determine if the first character is a numerical value so that the program doesnt break
                        choice2 = Integer.parseInt(input);
                    } else {
                        System.out.print("\nInvalid Input...\n");
                    }
                } while ((int) (input.charAt(0)) < 48 || (int) (input.charAt(0)) > 57);

                cart.get(choice1).setQuantity(choice2);
                break;
            case 1://remove from cart
                
                cart.get(choice1).setQuantity(0); //reset quantity
                cart.remove(choice1); 
                break;
            case 2:
                return;
            default: //this case should never happen
                System.out.print("\n\nHow did you get here?");
        }

    }

    static void printRecipt() {//prints recipt
        double total = 0;
        printCart();
        for (Item item : cart) {
            total += (item.getPrice() * item.getQuantity());
        }
        System.out.printf("Total Price: $%.2f\n\n", total);
    }

}
/*

     MAIN MENU
--------------------
1) Add Item to Cart
2) Manage Cart
3) Print shop items
4) Print cart
5) Print recipt
6) Exit
> 1
0) NAME: A Game of Thrones
PRICE: 4.99
AUTHOR: George RR Martin
SOFTCOVER, FICTION, USED

1) NAME: The Name of the Wind
PRICE: 10.99
AUTHOR: Patrick Rothfuss
HARDCOVER, NONFICTION, USED

2) NAME: A Discovery of Witches
PRICE: 19.99
AUTHOR: Deborah Harkness
HARDCOVER, FICTION, USED

3) NAME: The Blade Itself
PRICE: 9.60
AUTHOR: Joe Abercrombie
SOFTCOVER, NONFICTION, USED

4) NAME: Magnets...How Do They Work?
PRICE: 14.95
AUTHOR: Elvis Stevens
HARDCOVER, FICTION, NEW

5) NAME: Thaumonomicon
PRICE: 100.00
AUTHOR: Azanor the Void Walker
HARDCOVER, NONFICTION, NEW

6) NAME: Thinking With PoRTaLS
PRICE: 4.50
AUTHOR: Aperture Science
SOFTCOVER, NONFICTION, NEW

7) NAME: The Legend of Kara Tul
PRICE: 0.99
AUTHOR: Kara Tul
HARDCOVER, NONFICTION, USED

8) NAME: Visions of Bravery
PRICE: 8.49
AUTHOR: Elanor the Brave
SOFTCOVER, FICTION, USED

9) NAME: Childen of Morta
PRICE: 6.66
AUTHOR: Frankling Zappa
HARDCOVER, FICTION, NEW

10) NAME: Magic The Gathering
PRICE: 6.99
SET: Dark Ascension

11) NAME: Yu Ghi Oh!
PRICE: 12.99
SET: Secrets of Eternity

12) NAME: Cardfight!! Vanguard
PRICE: 4.95
SET: A Banquet of Divas

13) NAME: Pokemon
PRICE: 10.98
SET: Johto League

14) NAME: Weiss Schwarz
PRICE: 19.99
SET: Sword Art Online

15) Return to Main...

Which item would you like to add: 14

Item Added...
     MAIN MENU
--------------------
1) Add Item to Cart
2) Manage Cart
3) Print shop items
4) Print cart
5) Print recipt
6) Exit
> 1
0) NAME: A Game of Thrones
PRICE: 4.99
AUTHOR: George RR Martin
SOFTCOVER, FICTION, USED

1) NAME: The Name of the Wind
PRICE: 10.99
AUTHOR: Patrick Rothfuss
HARDCOVER, NONFICTION, USED

2) NAME: A Discovery of Witches
PRICE: 19.99
AUTHOR: Deborah Harkness
HARDCOVER, FICTION, USED

3) NAME: The Blade Itself
PRICE: 9.60
AUTHOR: Joe Abercrombie
SOFTCOVER, NONFICTION, USED

4) NAME: Magnets...How Do They Work?
PRICE: 14.95
AUTHOR: Elvis Stevens
HARDCOVER, FICTION, NEW

5) NAME: Thaumonomicon
PRICE: 100.00
AUTHOR: Azanor the Void Walker
HARDCOVER, NONFICTION, NEW

6) NAME: Thinking With PoRTaLS
PRICE: 4.50
AUTHOR: Aperture Science
SOFTCOVER, NONFICTION, NEW

7) NAME: The Legend of Kara Tul
PRICE: 0.99
AUTHOR: Kara Tul
HARDCOVER, NONFICTION, USED

8) NAME: Visions of Bravery
PRICE: 8.49
AUTHOR: Elanor the Brave
SOFTCOVER, FICTION, USED

9) NAME: Childen of Morta
PRICE: 6.66
AUTHOR: Frankling Zappa
HARDCOVER, FICTION, NEW

10) NAME: Magic The Gathering
PRICE: 6.99
SET: Dark Ascension

11) NAME: Yu Ghi Oh!
PRICE: 12.99
SET: Secrets of Eternity

12) NAME: Cardfight!! Vanguard
PRICE: 4.95
SET: A Banquet of Divas

13) NAME: Pokemon
PRICE: 10.98
SET: Johto League

14) NAME: Weiss Schwarz
PRICE: 19.99
SET: Sword Art Online

15) Return to Main...

Which item would you like to add: 14

Item Added...
     MAIN MENU
--------------------
1) Add Item to Cart
2) Manage Cart
3) Print shop items
4) Print cart
5) Print recipt
6) Exit
> 1
0) NAME: A Game of Thrones
PRICE: 4.99
AUTHOR: George RR Martin
SOFTCOVER, FICTION, USED

1) NAME: The Name of the Wind
PRICE: 10.99
AUTHOR: Patrick Rothfuss
HARDCOVER, NONFICTION, USED

2) NAME: A Discovery of Witches
PRICE: 19.99
AUTHOR: Deborah Harkness
HARDCOVER, FICTION, USED

3) NAME: The Blade Itself
PRICE: 9.60
AUTHOR: Joe Abercrombie
SOFTCOVER, NONFICTION, USED

4) NAME: Magnets...How Do They Work?
PRICE: 14.95
AUTHOR: Elvis Stevens
HARDCOVER, FICTION, NEW

5) NAME: Thaumonomicon
PRICE: 100.00
AUTHOR: Azanor the Void Walker
HARDCOVER, NONFICTION, NEW

6) NAME: Thinking With PoRTaLS
PRICE: 4.50
AUTHOR: Aperture Science
SOFTCOVER, NONFICTION, NEW

7) NAME: The Legend of Kara Tul
PRICE: 0.99
AUTHOR: Kara Tul
HARDCOVER, NONFICTION, USED

8) NAME: Visions of Bravery
PRICE: 8.49
AUTHOR: Elanor the Brave
SOFTCOVER, FICTION, USED

9) NAME: Childen of Morta
PRICE: 6.66
AUTHOR: Frankling Zappa
HARDCOVER, FICTION, NEW

10) NAME: Magic The Gathering
PRICE: 6.99
SET: Dark Ascension

11) NAME: Yu Ghi Oh!
PRICE: 12.99
SET: Secrets of Eternity

12) NAME: Cardfight!! Vanguard
PRICE: 4.95
SET: A Banquet of Divas

13) NAME: Pokemon
PRICE: 10.98
SET: Johto League

14) NAME: Weiss Schwarz
PRICE: 19.99
SET: Sword Art Online

15) Return to Main...

Which item would you like to add: 3

Item Added...
     MAIN MENU
--------------------
1) Add Item to Cart
2) Manage Cart
3) Print shop items
4) Print cart
5) Print recipt
6) Exit
> 6
0) NAME: Weiss Schwarz
PRICE: 19.99
SET: Sword Art Online
QTY: 2

1) NAME: The Blade Itself
PRICE: 9.60
AUTHOR: Joe Abercrombie
SOFTCOVER, NONFICTION, USED
QTY: 1

Total Price: $49.58

Thank you for shopping with us, have a nice day!

*/