/*********************************************************
 * file name: Book.java 
 * Author: Dylan Kozicki, Ryan Ousey, Zachary McMinn
 * Date of Last Revision: 10/7/2014
 * 
 * Class Name: Book
 * Short Description: Extends Item with the variables isHardcover, isFiction, isUsed and author.
 *********************************************************/

package bookstore;

public class Book extends Item {
    
    private boolean isHardcover, isFiction, isUsed; //booleans used to describe the book
    private String author; //author of the book

    public Book() {
        this("Untitled",-1,true,true,false,"Anonymous");
    }
    
    //most advanced construtor, all other constructors call this to instantiate the object
    public Book(String name, double price, boolean isHC, boolean isFict, boolean isUsed, String author){
        this.name = name;
        this.price = price;
        isHardcover = isHC;
        isFiction = isFict;
        this.isUsed = isUsed;
        this.author = author;
        quantity = 0;

    }
    
    public Book(String name, double price, String author){
        this(name,price,true,true,false,author);
    }

    public Book(String name, double price) {
        this(name, price,true,true,false,"Anonymous");
    }
        
    public Book(String name){
        this(name,-1,true,true,false,"Anonymous");
    }
    
    public boolean getHardcover(){
        return isHardcover;
    }
    
    public boolean getFiction(){
        return isFiction;
    }
    
    public boolean getUsed(){
        return isUsed;
    }
    
    public String getAuthor(){
        return author;
    }
    
    public void setHardcover(boolean hardcover){
        isHardcover = hardcover;
    }
    
    public void setFiction(boolean fiction){
        isFiction = fiction;
    }
    
    public void setUsed(boolean used){
        isUsed = used;
    }
    
    public void setAuthor(String author){
        this.author = author;
    }
    
    @Override
    public String toString(){
        return "NAME: " + name + "\nPRICE: " + format.format(price) + "\n"
                + "AUTHOR: " + author + "\n"
                + (isHardcover ? "HARDCOVER, " : "SOFTCOVER, ")
                + (isFiction ? "FICTION, " : "NONFICTION, ")
                + (isUsed ? "USED\n" : "NEW\n");
    }

}
