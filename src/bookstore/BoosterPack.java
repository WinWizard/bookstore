/*********************************************************
  * File Name: BoosterPack.java
  * Author: Dylan Kozicki, Ryan Ousey, Zachary McMinn
  * Date of Last Revision: 10/7/2014
  *
  * Description: Extends Item to have a "set" variable along with the name and price. 
  * 
  * Class created: BoosterPack
  * 
  * //Available Constructors:
  * 
  * BoosterPack()
  * BoosterPack(String name)
  * BoosterPack(String name, String newSet)
  * BoosterPack(String name, double price, String newSet)
  * 
  * Available Functions:
  * void getSet(String newSet)
  * //Changes set to be newSet
  * 
  * String setSet()
  * //Returns set
  * 
  * String toString()
  * //Returns  NAME: (name) 
  *            PRICE: (price)
               SET: (set)
 **********************************************************/

package bookstore;

public class BoosterPack extends Item {

    protected String set;
    
    public BoosterPack() {
        this("Unnamed pack",-1, "Unknown Set");
    }

    //most advanced construtor, all other constructors call this to instantiate the object
    public BoosterPack(String name, double price, String newSet) {
       this.name = name;
       this.price = price;
       set=newSet;
       quantity = 0;
    }

    public BoosterPack(String name) {
        this(name,-1,"Unknown Set");
    }
    
    public BoosterPack(String name, String newSet){
        this(name,-1,newSet);
    }
    
    public String getSet(){
        return set;
    }
    
    public void setSet(String newSet){
        set=newSet;
    }

    @Override
    public String toString(){
        return "NAME: " + name + "\nPRICE: " + format.format(price) + "\n"
                + "SET: " + set + "\n";
    }

}
