/*********************************************************
 * File Name: Item.java
 * Author: Dylan Kozicki, Ryan Ousey, Zachary McMinn
 * Date of Last Revision: 10/7/2014
 * 
 * Class Name: Item
 * Short Description: A class containing a price and a name. 
 *                    Intended to be a superclass for several 
 *                    subtypes of item. Including Book and BoosterPack
 **********************************************************/

package bookstore;

import java.text.DecimalFormat;

public abstract class Item {
protected String name; //name of the item
protected double price; //price of the item
protected int quantity; //quantity of the item for the cart
protected DecimalFormat format = new DecimalFormat("0.00"); //format for a dollar value use for tostring

public void setQuantity(int q){
    quantity = q;
}

public void addQuantity(int q){
    quantity +=q;
}

public int removeQuantity(int q){
    if(quantity - q <= 0){
        quantity = 0;
        return -1;
    }
    quantity -=q;
    return q;
}
public int getQuantity(){
    return quantity;
}

public void setName(String name){
    this.name = name;
}

public String getName(){
    return name;
}

public void setPrice(double price){
    this.price = price;
}

public double getPrice(){
    return price;
}

@Override
public String toString(){
    return "NAME: " + name + "\nPRICE: " + format.format(price) + "\n";
}

}
